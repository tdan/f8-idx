<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://fifty8.co
 * @since             1.0.0
 * @package           F8_Idx
 *
 * @wordpress-plugin
 * Plugin Name:       Fifty8 IDX Real Estate Plugin
 * Plugin URI:        https://fifty8.co
 * Description:       Display and search your Real Estate listings directly on your WordPress website.  Support for majority of MLS providers in North America. Set up in less than 5 minutes! 
 * Version:           1.0.0
 * Author:            Trung An
 * Author URI:        https://fifty8.co
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       f8-idx
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'F8_IDX_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-f8-idx-activator.php
 */
function activate_f8_idx() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-f8-idx-activator.php';
	F8_Idx_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-f8-idx-deactivator.php
 */
function deactivate_f8_idx() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-f8-idx-deactivator.php';
	F8_Idx_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_f8_idx' );
register_deactivation_hook( __FILE__, 'deactivate_f8_idx' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-f8-idx.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_f8_idx() {

	$plugin = new F8_Idx();
	$plugin->run();

}
run_f8_idx();
