<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://fifty8.co
 * @since      1.0.0
 *
 * @package    F8_Idx
 * @subpackage F8_Idx/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
