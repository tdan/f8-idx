<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://fifty8.co
 * @since      1.0.0
 *
 * @package    F8_Idx
 * @subpackage F8_Idx/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    F8_Idx
 * @subpackage F8_Idx/includes
 * @author     Trung An <fifty8co@gmail.com>
 */
class F8_Idx_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
