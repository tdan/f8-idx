<?php

/**
 * Fired during plugin activation
 *
 * @link       https://fifty8.co
 * @since      1.0.0
 *
 * @package    F8_Idx
 * @subpackage F8_Idx/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    F8_Idx
 * @subpackage F8_Idx/includes
 * @author     Trung An <fifty8co@gmail.com>
 */
class F8_Idx_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
